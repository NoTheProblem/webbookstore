package com.example.demo.controller;

import com.example.demo.entity.Dostavljac;
import com.example.demo.entity.Kupac;
import com.example.demo.entity.dto.LoginUserDTO;
import com.example.demo.service.WebService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

@RequestMapping(value = "/login")
public class LoginController {

    private final WebService webService;

    public LoginController(WebService webService) {
        this.webService = webService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginUserDTO> loginUser(@RequestBody LoginUserDTO loginUserDTO) throws Exception{
        List<Kupac> kupac = webService.findUPKupac(loginUserDTO.getKorIme(), loginUserDTO.getSifra());
        if(kupac.isEmpty()){
            List<Dostavljac> dostavljacList = webService.findUPDostavljac(loginUserDTO.getKorIme(), loginUserDTO.getSifra());
            if(dostavljacList.isEmpty()){
                return new ResponseEntity<>(loginUserDTO, HttpStatus.OK);
            }
            loginUserDTO.setUloga("dostavljac");
            return new ResponseEntity<>(loginUserDTO, HttpStatus.OK);
        }
        Kupac kupcic = kupac.get(0);
        loginUserDTO.setUloga(kupcic.getUloga());
        return new ResponseEntity<>(loginUserDTO, HttpStatus.OK);
    }
}
