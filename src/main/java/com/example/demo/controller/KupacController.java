package com.example.demo.controller;

import com.example.demo.entity.Korpa;
import com.example.demo.entity.Kupac;
import com.example.demo.entity.dto.KupacDTO;
import com.example.demo.service.WebService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping(value = "/kupci")
public class KupacController {

    private final WebService webService;

    public KupacController(WebService webService) {
        this.webService= webService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> kreiratiKupca(@RequestBody KupacDTO kupacDTO) throws Exception{
        Kupac kupac = new Kupac(kupacDTO.getKorIme(), kupacDTO.getLozinka(), kupacDTO.getIme(),kupacDTO.getPrezime(), kupacDTO.getTelefon(), kupacDTO.getEmail(), kupacDTO.getGrad(), kupacDTO.getAdresa());
        Kupac novKupac = webService.create(kupac);
        Korpa korpa = new Korpa();
        korpa.setStanje("Napravljena");
        novKupac.setKorpaKupac(korpa);
        KupacDTO novKupacDTO = new KupacDTO(novKupac.getId(),novKupac.getKorIme(), novKupac.getLozinka(), novKupac.getIme(),novKupac.getPrezime(), novKupac.getTelefon(), novKupac.getEmail(), novKupac.getGrad(), novKupac.getAdresa());
        return new ResponseEntity<>(novKupacDTO, HttpStatus.OK);
    }
}