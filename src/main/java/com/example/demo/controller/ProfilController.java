package com.example.demo.controller;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Korpa;
import com.example.demo.entity.Kupac;
import com.example.demo.entity.dto.ArtikalDTO;
import com.example.demo.entity.dto.KupacDTO;
import com.example.demo.repository.ArtikalRepository;
import com.example.demo.repository.KorpaRepository;
import com.example.demo.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController

@RequestMapping(value = "/profil")
public class ProfilController {

    @Autowired
    private ArtikalRepository artikalRepository;

    @Autowired
    private KorpaRepository korpaRepository;

    private final WebService webService;

    public ProfilController(WebService webService) {
        this.webService = webService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> getKupac(@RequestBody KupacDTO kupacDTO) {
        List<Kupac> kupaci = webService.findUKupac(kupacDTO.getKorIme());
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
        }
        Kupac novKupac = kupaci.get(0);
        KupacDTO novKupacDTO = new KupacDTO(novKupac.getId(),novKupac.getKorIme(), novKupac.getLozinka(), novKupac.getIme(),novKupac.getPrezime(), novKupac.getTelefon(), novKupac.getEmail(), novKupac.getGrad(), novKupac.getAdresa());
        novKupacDTO.setKupon(novKupac.getKupon());
        return new ResponseEntity<>(novKupacDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/omiljeni/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getArtikal(@PathVariable(name = "korIme") String korIme) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(korIme);
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        List<Artikal> artikals = webService.findOmiljeneArtikle(kupac.getId());
        for(Artikal art : artikals){
            ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
            artikalDTOList.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/korpa/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getKorpa(@PathVariable(name = "korIme") String korIme) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(korIme);
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        Korpa korpa = kupac.getKorpaKupac();
        List<Artikal> artikals = korpa.getLista();
        for(Artikal art : artikals){
            ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
            artikalDTOList.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/istorija/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getIstorija(@PathVariable(name = "korIme") String korIme) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(korIme);
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        List<Korpa> korpe = kupac.getKupljeneKorpe();
        for(int i = 0; i < korpe.size();i++) {
            Korpa korpa = korpe.get(i);
            List<Artikal> artikals = korpa.getLista();
            for (Artikal art : artikals) {
                ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
                artikalDTOList.add(artikalDTO);
            }
        }
        return new ResponseEntity<>(artikalDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/kupi",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> kupiKorpu(@RequestBody KupacDTO kupacDTO) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(kupacDTO.getKorIme());
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        Korpa korpa = kupac.getKorpaKupac();
        int cena = korpa.nadjiCenuKorpe();
        if(cena>=1000){
            kupac.setKupon(kupac.getKupon()+1);
        }
        korpa.setCenaKorpe(cena);
        korpa.setStanje("Kupljeno");
        List<Korpa>  korpaList = kupac.getKupljeneKorpe();
        korpaList.add(korpa);
        kupac.setKupljeneKorpe(korpaList);
        Korpa korpa1= new Korpa();
        korpa1.setStanje("Napravljena");
        Date date = new Date();
        korpa1.setDate(date);
        kupac.setKorpaKupac(korpa1);
        korpaRepository.save(korpa1);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/kupi/{popust}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> kupiKorpuSaPopustom(@RequestBody KupacDTO kupacDTO,@PathVariable(name = "popust") int popust) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(kupacDTO.getKorIme());
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        Korpa korpa = kupac.getKorpaKupac();
        double pop ;
        if(popust==5){
             pop =  0.9;
             kupac.setKupon(kupac.getKupon()-5);
        }else {
             pop =  0.8;
            kupac.setKupon(kupac.getKupon()-10);
        }
        int cena;
        cena = (int) (korpa.nadjiCenuKorpe()*pop);
        if(cena>=1000){
            kupac.setKupon(kupac.getKupon()+1);
        }
        korpa.setStanje("Kupljeno");
        korpa.setCenaKorpe(cena);
        List<Korpa>  korpaList = kupac.getKupljeneKorpe();
        korpaList.add(korpa);
        kupac.setKupljeneKorpe(korpaList);
        Korpa korpa1= new Korpa();
        korpa1.setStanje("Napravljena");
        Date date = new Date();
        korpa1.setDate(date);
        kupac.setKorpaKupac(korpa1);
        korpaRepository.save(korpa1);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> izbrisiARtikal(@RequestBody KupacDTO kupacDTO,@PathVariable(name = "id") Long idArtikla) {
        List<ArtikalDTO> artikalDTOList = new ArrayList<>();
        List<Kupac> kupaci = webService.findUKupac(kupacDTO.getKorIme());
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
        }
        Kupac kupac = kupaci.get(0);
        Korpa korpa = kupac.getKorpaKupac();
        List<Artikal> korpas = korpa.getLista();
        for(Artikal artikal:korpas){
            if(artikal.getId()==idArtikla){
                Artikal a = webService.findOneArtikal(idArtikla);
                a.setKolicina(a.getKolicina()+1);
                korpas.remove(a);
                break;
            }
        }
        korpa.setKorpa(korpas);
        kupac.setKorpaKupac(korpa);
        korpaRepository.save(korpa);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }
}