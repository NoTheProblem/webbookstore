package com.example.demo.controller;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Dostavljac;
import com.example.demo.entity.Korpa;
import com.example.demo.entity.Kupac;
import com.example.demo.entity.dto.ArtikalDTO;
import com.example.demo.entity.dto.DostavljacDTO;
import com.example.demo.entity.dto.KorpaDTO;
import com.example.demo.entity.dto.KupacDTO;
import com.example.demo.repository.ArtikalRepository;
import com.example.demo.repository.DostavljacRepository;
import com.example.demo.repository.KorpaRepository;
import com.example.demo.repository.KupacRepository;
import com.example.demo.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController

@RequestMapping(value = "/admin")
public class AdminController {
    @Autowired
    private KupacRepository kupacRepository;

    @Autowired
    private ArtikalRepository artikalRepository;

    @Autowired
    private KorpaRepository korpaRepository;

    @Autowired
    private DostavljacRepository dostavljacRepository;

    private final WebService webService;

    public AdminController(WebService webService) {
        this.webService = webService;
    }

    @GetMapping(value = "/prikaziArtikal", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getArtikal() {
        List<Artikal> artikli = this.webService.findAllArtikal();
        List<ArtikalDTO> artikalsDTOS  = new ArrayList<>();
        for(Artikal art : artikli){
            ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
            artikalsDTOS.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalsDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/prikaziKupac", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KupacDTO>> getKupac() {
        List<Kupac> kupacs = this.webService.findAllKupac();
        List<KupacDTO> kupacDTOList = new ArrayList<>();
        for(Kupac kup : kupacs){
            KupacDTO novKupacDTO = new KupacDTO(kup.getId(),kup.getKorIme(), kup.getLozinka(), kup.getIme(),kup.getPrezime(), kup.getTelefon(), kup.getEmail(), kup.getGrad(), kup.getAdresa());
            kupacDTOList.add(novKupacDTO);
        }
        return new ResponseEntity<>(kupacDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/prikaziDostavljac", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DostavljacDTO>> getDostavljac() {
        List<Dostavljac> dostavljacs = this.webService.findAllDostavljac();
        List<DostavljacDTO> dostavljacDTOS = new ArrayList<>();
        for(Dostavljac dos : dostavljacs){
            DostavljacDTO novDostavljacDTO = new DostavljacDTO(dos.getId(),dos.getKorIme(), dos.getLozinka(), dos.getIme(),dos.getPrezime(),dos.getUloga(), dos.getTelefon(), dos.getEmail(), dos.getGrad(), dos.getAdresa());
            dostavljacDTOS.add(novDostavljacDTO);
        }
        return new ResponseEntity<>(dostavljacDTOS, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteA/{id}")
    public void deleteArtikal(@PathVariable(name = "id") Long id) {
        webService.deleteArtikal(id);
    }

    @DeleteMapping(value = "/deleteK/{id}")
    public void deleteKorisnik(@PathVariable(name = "id") Long id) {
        webService.deleteKupac(id);
    }

    @DeleteMapping(value = "/deleteD/{id}")
    public void deleteDostavljac(@PathVariable(name = "id") Long id) {
        webService.deleteDostavljac(id);
    }

    @PostMapping(value = "/changeUlogaK/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> changeKupacToDostavljac(@RequestBody KupacDTO kupacDTO, @PathVariable(name = "id") Long id) throws Exception {
        System.out.println("radi");
        Kupac kupac = webService.findOneKupac(id);
        Dostavljac dostavljac = new Dostavljac(kupac.getKorIme(), kupac.getLozinka(), kupac.getIme(),kupac.getPrezime(),"dostavljac",  kupac.getTelefon(), kupac.getEmail(), kupac.getGrad(), kupac.getAdresa());
        webService.deleteKupac(id);
        dostavljacRepository.save(dostavljac);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/changeUlogaD/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> changeDostavljacToKupac(@RequestBody KupacDTO kupacDTO,@PathVariable(name = "id") Long id) throws Exception {
        Dostavljac dostavljac = webService.findOneDostavljac(id);
        Kupac kupac = new Kupac(dostavljac.getKorIme(), dostavljac.getLozinka(), dostavljac.getIme(),dostavljac.getPrezime(),  dostavljac.getTelefon(), dostavljac.getEmail(), dostavljac.getGrad(), dostavljac.getAdresa());
        webService.create(kupac);
        webService.deleteDostavljac(id);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/addArtikal", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArtikalDTO> addArtikal(@RequestBody ArtikalDTO artikalDTO) throws Exception {
        Artikal artikal = new Artikal(artikalDTO.getNaziv(),artikalDTO.getOpis(),artikalDTO.getCena(),artikalDTO.getKolicina(),artikalDTO.getKategorija());
        artikalRepository.save(artikal);
        return new ResponseEntity<>(artikalDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/izvestaj/{period}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KorpaDTO>> getIzvestaj(@PathVariable(name = "period") String period) {
        List<KorpaDTO> korpaDTOList = new ArrayList<>();
        List<Korpa> korpaList = webService.sveKorpeStanje("Dostavljeno");
        Date pocetni = new Date();
        Date krajnji = new Date();
        if(period=="dnevni"){
            pocetni.setHours(krajnji.getHours()-24);
        }else {
            if(period=="mesec"){
                pocetni.setMonth(krajnji.getMonth()-1);
            }
            else {
                pocetni.setYear(krajnji.getYear()-1);
            }
        }
        for(Korpa kor: korpaList){
            if( !kor.getDate().before(pocetni) && !kor.getDate().after(krajnji)){
                //kor.setCenaKorpe(kor.getCenaKorpe());
                KorpaDTO korpaDTO = new KorpaDTO(kor.getId(),kor.getDate(),kor.getStanje(),kor.getCenaKorpe());
                korpaDTOList.add(korpaDTO);
            }
        }
        return new ResponseEntity<>(korpaDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/changeA/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArtikalDTO> getArtikalForChange(@PathVariable(name = "id") Long id) {
        Artikal artikal = webService.findOneArtikal(id);
        ArtikalDTO artikalDTO = new ArtikalDTO(artikal.getId(),artikal.getNaziv(),artikal.getCena(),artikal.getOpis(),artikal.getKategorija(),artikal.getKolicina(),artikal.isPopust());
        return new ResponseEntity<>(artikalDTO,HttpStatus.OK);
    }

    @PostMapping(value = "/changeArtikal", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArtikalDTO> changeArtikal(@RequestBody ArtikalDTO artikalDTO) throws Exception {
        Artikal artikal = webService.findOneArtikal(artikalDTO.getId());
        if(artikal == null){
            return new ResponseEntity<>(artikalDTO,HttpStatus.OK);
        }
        artikal.setKolicina(artikalDTO.getKolicina());
        artikal.setCena(artikalDTO.getCena());
        artikal.setKategorija(artikalDTO.getKategorija());
        artikal.setNaziv(artikalDTO.getNaziv());
        artikal.setOpis(artikalDTO.getOpis());
        artikalRepository.save(artikal);
        return  new ResponseEntity<>(artikalDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/changeD/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DostavljacDTO> getDostavljacForChange(@PathVariable(name = "id") Long id) {
        Dostavljac dostavljac = webService.findOneDostavljac(id);
        DostavljacDTO dostavljacDTO = new DostavljacDTO(dostavljac.getId(),dostavljac.getKorIme(),dostavljac.getLozinka(),dostavljac.getIme(),dostavljac.getPrezime(),dostavljac.getUloga(),dostavljac.getTelefon(),dostavljac.getGrad(),dostavljac.getEmail(),dostavljac.getAdresa());
        return new ResponseEntity<>(dostavljacDTO,HttpStatus.OK);
    }

    @PostMapping(value = "/changeDostavljac",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DostavljacDTO> changeDostavljac(@RequestBody DostavljacDTO dostavljacDTO){
        Dostavljac dostavljac =webService.findOneDostavljac(dostavljacDTO.getId());
        dostavljac.setAdresa(dostavljacDTO.getAdresa());
        dostavljac.setEmail(dostavljacDTO.getEmail());
        dostavljac.setGrad(dostavljacDTO.getGrad());
        dostavljac.setIme(dostavljacDTO.getIme());
        dostavljac.setKorIme(dostavljacDTO.getKorIme());
        dostavljac.setLozinka(dostavljacDTO.getLozinka());
        dostavljac.setPrezime(dostavljacDTO.getPrezime());
        dostavljac.setTelefon(dostavljacDTO.getTelefon());
        dostavljacRepository.save(dostavljac);
        return  new ResponseEntity<>(dostavljacDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/changeK/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> getKupacForChange(@PathVariable(name = "id") Long id) {
       Kupac kupac = webService.findOneKupac(id);
        KupacDTO kupacDTO= new KupacDTO(kupac.getId(),kupac.getKorIme(),kupac.getLozinka(),kupac.getIme(),kupac.getPrezime(),kupac.getTelefon(),kupac.getGrad(),kupac.getEmail(),kupac.getAdresa());
        return new ResponseEntity<>(kupacDTO,HttpStatus.OK);
    }

    @PostMapping(value = "/changeKupac",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> changeKupac(@RequestBody KupacDTO kupacDTO){
        System.out.println(kupacDTO.getId());
        Kupac kupac =webService.findOneKupac(kupacDTO.getId());
        System.out.println(kupacDTO.getId());
        if(kupac==null){
            return new ResponseEntity<>(kupacDTO, HttpStatus.BAD_REQUEST);
        }
        kupac.setAdresa(kupacDTO.getAdresa());
        kupac.setEmail(kupacDTO.getEmail());
        kupac.setGrad(kupacDTO.getGrad());
        kupac.setIme(kupacDTO.getIme());
        kupac.setKorIme(kupacDTO.getKorIme());
        kupac.setLozinka(kupacDTO.getLozinka());
        kupac.setPrezime(kupacDTO.getPrezime());
        kupac.setTelefon(kupacDTO.getTelefon());
        kupacRepository.save(kupac);
        return  new ResponseEntity<>(kupacDTO,HttpStatus.OK);
    }
}