package com.example.demo.controller;

import com.example.demo.entity.Dostavljac;
import com.example.demo.entity.Korpa;
import com.example.demo.entity.dto.DostavljacDTO;
import com.example.demo.entity.dto.KorpaDTO;
import com.example.demo.service.WebService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController

@RequestMapping(value = "/dostavljac")
public class DostavljacController {

    private final WebService webService;

    public DostavljacController(WebService webService) {
        this.webService = webService;
    }

    @GetMapping(value = "/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DostavljacDTO> getDostavljac(@PathVariable(name = "korIme") String korIme) {
        List<Dostavljac> dostavljacList = webService.findUDostavljac(korIme);
        if(dostavljacList.isEmpty()){
            DostavljacDTO dostavljacDTO = new DostavljacDTO();
            return new ResponseEntity<>(dostavljacDTO, HttpStatus.OK);
        }
        Dostavljac dostavljac = dostavljacList.get(0);
        DostavljacDTO dostavljacDTO = new DostavljacDTO(dostavljac.getId(),dostavljac.getKorIme(), dostavljac.getLozinka(), dostavljac.getIme(),dostavljac.getPrezime(), dostavljac.getUloga(),  dostavljac.getTelefon(), dostavljac.getEmail(), dostavljac.getGrad(), dostavljac.getAdresa());
        return  new ResponseEntity<>(dostavljacDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/korpe/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KorpaDTO>> getKorpa(@PathVariable(name = "korIme") String korIme) {
        List<KorpaDTO> korpaDTOList = new ArrayList<>();
        List<Korpa> korpaList = webService.sveKorpeStanje("Kupljeno");
        if(korpaList.isEmpty()){
            return new ResponseEntity<>(korpaDTOList,HttpStatus.BAD_REQUEST);
        }
        for(Korpa kor : korpaList){
            KorpaDTO korpaDTO = new KorpaDTO(kor.getId(),kor.getDate(),kor.getStanje());
            korpaDTOList.add(korpaDTO);
        }
        return new ResponseEntity<>(korpaDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/preuzmi/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DostavljacDTO> dodajKorpa(@RequestBody DostavljacDTO dostavljacDTO,@PathVariable(name = "id") Long idKorpe) throws Exception {
        List<Dostavljac> dostavljacList = webService.findUDostavljac(dostavljacDTO.getKorIme());
        if(dostavljacList.isEmpty()){
            return  new ResponseEntity<>(dostavljacDTO,HttpStatus.OK);
        }
        Dostavljac dostavljac = dostavljacList.get(0);
        webService.dobavljaj(dostavljac.getId(), idKorpe);
        return new ResponseEntity<>(dostavljacDTO, HttpStatus.OK);
    }

    //TODO
    @GetMapping(value = "/preuzete/{korIme}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KorpaDTO>> getDostava(@PathVariable(name = "korIme") String korIme) {
        List<KorpaDTO> korpaDTOList = new ArrayList<>();
        List<Dostavljac> dostavljaci = webService.findUDostavljac(korIme);
        Dostavljac dostavljac = dostavljaci.get(0);
        List<Korpa> korpaList = webService.sveKorpeStanje("Dostava u toku");
        if(korpaList.isEmpty()){
            return new ResponseEntity<>(korpaDTOList,HttpStatus.BAD_REQUEST);
        }
        for(Korpa kor : korpaList){
            if(kor.getIdDostavljaca()==dostavljac.getId()) {

                KorpaDTO korpaDTO = new KorpaDTO(kor.getId(), kor.getDate(), kor.getStanje());
                korpaDTOList.add(korpaDTO);
            }
        }
        return new ResponseEntity<>(korpaDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/dostavi/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DostavljacDTO> dostavljenaKorpa(@RequestBody DostavljacDTO dostavljacDTO,@PathVariable(name = "id") Long idKorpe) throws Exception {
        List<Dostavljac> dostavljacList = webService.findUDostavljac(dostavljacDTO.getKorIme());
        if(dostavljacList.isEmpty()){
            return  new ResponseEntity<>(dostavljacDTO,HttpStatus.OK);
        }
        Dostavljac dostavljac = dostavljacList.get(0);
        webService.dostavi(dostavljac.getId(),idKorpe);
        return new ResponseEntity<>(dostavljacDTO, HttpStatus.OK);
    }
}