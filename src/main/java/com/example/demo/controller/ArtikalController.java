package com.example.demo.controller;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Kupac;
import com.example.demo.entity.dto.ArtikalDTO;
import com.example.demo.entity.dto.KupacDTO;
import com.example.demo.service.WebService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController

@RequestMapping(value = "/kategorija")
public class ArtikalController {

    private final WebService webService;

    public ArtikalController(WebService webService) {
        this.webService = webService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getArtikal() {
        List<Artikal> artikli = this.webService.findAllArtikal();
        List<ArtikalDTO> artikalsDTOS  = new ArrayList<>();
        for(Artikal art : artikli){
            ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
            artikalsDTOS.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalsDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArtikalDTO>> getArtikalic(@PathVariable(name = "id") Long id) {
        String kat;
        if(id==0){
             kat= "beletristika";
        }else {
            if(id==2){
                 kat= "drama";
            }else {
                 kat= "strucna";
            }
        }
        List<Artikal> artikals = this.webService.findByKategorija(kat);
        List<ArtikalDTO> artikalsDTOS  = new ArrayList<>();
        for(Artikal art : artikals){
            ArtikalDTO artikalDTO = new ArtikalDTO(art.getId(), art.getNaziv(), art.getCena(), art.getKategorija(), art.getOpis(), art.getKolicina(), art.isPopust());
            artikalsDTOS.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalsDTOS, HttpStatus.OK);
    }

    @PostMapping(value = "/omiljeno/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> dodajOmiljeno(@RequestBody KupacDTO kupacDTO,@PathVariable(name = "id") Long idArtikla) throws Exception {
        List<Kupac> kupaci =  webService.findUKupac(kupacDTO.getKorIme());
        if(kupaci.isEmpty()){
            return new ResponseEntity<>(kupacDTO, HttpStatus.BAD_REQUEST);
        }
        Kupac kupac = kupaci.get(0);
        webService.setOmiljeniArtikal(kupac.getId(), idArtikla);
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/korpa/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KupacDTO> dodajKorpa(@RequestBody KupacDTO kupacDTO,@PathVariable(name = "id") Long idArtikla) throws Exception {
        List<Kupac> kupaci = webService.findUKupac(kupacDTO.getKorIme());
        if (kupaci.isEmpty()) {
            return new ResponseEntity<>(kupacDTO, HttpStatus.BAD_REQUEST);
        }
        Kupac kupac = kupaci.get(0);
        if(!webService.setDodajUlistu(kupac.getId(), idArtikla)){
            return new ResponseEntity<>(kupacDTO, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(kupacDTO, HttpStatus.OK);
    }
}