package com.example.demo.entity.dto;

public class LoginUserDTO {

    private String korIme;
    private String sifra;
    private String uloga;

    public  LoginUserDTO(){

    }

    public LoginUserDTO(String korIme, String sifra, String uloga) {
        this.korIme = korIme;
        this.sifra = sifra;
        this.uloga = uloga;
    }

    public String getKorIme() {
        return korIme;
    }

    @Override
    public String toString() {
        return "LoginUserDTO{" +
                "korIme='" + korIme + '\'' +
                ", sifra='" + sifra + '\'' +
                ", uloga='" + uloga + '\'' +
                '}';
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }
}