package com.example.demo.entity.dto;

public class ArtikalDTO {

    private Long id;

    private String naziv;

    private String opis;

    private int cena;

    private int kolicina;

    private String kategorija;

    private boolean popust;

    public  ArtikalDTO(){

    }

    public ArtikalDTO(Long id,String naziv,  int cena,String opis, String kategorija, int kolicina, boolean popust) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.cena = cena;
        this.kolicina = kolicina;
        this.kategorija = kategorija;
        this.popust = popust;
    }


    @Override
    public String toString() {
        return "ArtikalDTO{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                ", opis='" + opis + '\'' +
                ", cena='" + cena + '\'' +
                ", kolicina=" + kolicina +
                ", kategorija='" + kategorija + '\'' +
                ", popust=" + popust +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public boolean isPopust() {
        return popust;
    }

    public void setPopust(boolean popust) {
        this.popust = popust;
    }
}
