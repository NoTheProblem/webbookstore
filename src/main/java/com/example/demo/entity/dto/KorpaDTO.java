package com.example.demo.entity.dto;

import java.util.Date;

public class KorpaDTO {


    private Long id;

    private Date datum;

    protected String stanje;

    private int cenaKorpe;

    public KorpaDTO() {

    }

    public KorpaDTO(Long id, Date datum, String stanje) {
        this.id = id;
        this.datum = datum;
        this.stanje = stanje;
    }

    public KorpaDTO(Long id, Date datum, String stanje, int cenaKorpe) {
        this.id = id;
        this.datum = datum;
        this.stanje = stanje;
        this.cenaKorpe=cenaKorpe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getStanje() {
        return stanje;
    }

    public void setStanje(String stanje) {
        this.stanje = stanje;
    }

    public int getCenaKorpe() {
        return cenaKorpe;
    }

    public void setCenaKorpe(int cenaKorpe) {
        this.cenaKorpe = cenaKorpe;
    }

    @Override
    public String toString() {
        return "KorpaDTO{" +
                "id=" + id +
                ", datum=" + datum +
                ", stanje='" + stanje + '\'' +
                '}';
    }
}


