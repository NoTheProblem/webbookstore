package com.example.demo.entity.dto;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Korpa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KupacDTO {

    protected Long id;

    protected String korIme;

    protected String lozinka;

    protected String ime;

    protected String prezime;

    protected String uloga;

    protected String telefon;

    protected String email;

    protected String grad;

    protected String adresa;

    protected Long dostavljaKorpu;

    protected Long kupon;

    private List<Artikal> omiljeniArtikli = new ArrayList<Artikal>();

    private List<Korpa> kupljeneKorpe = new ArrayList<Korpa>();

    private Set<Artikal> prethodniArtikli = new HashSet<>();

    public KupacDTO(Long id, String korIme, String lozinka, String ime, String prezime, String telefon, String email, String grad, String adresa) {
        this.id = id;
        this.korIme = korIme;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.telefon = telefon;
        this.email = email;
        this.grad = grad;
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "KupacDTO{" +
                "korIme='" + korIme + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                ", grad='" + grad + '\'' +
                ", adresa='" + adresa + '\'' +
                ", dostavljaKorpu=" + dostavljaKorpu +
                ", kupon=" + kupon +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Long getDostavljaKorpu() {
        return dostavljaKorpu;
    }

    public void setDostavljaKorpu(Long dostavljaKorpu) {
        this.dostavljaKorpu = dostavljaKorpu;
    }

    public Long getKupon() {
        return kupon;
    }

    public void setKupon(Long kupon) {
        this.kupon = kupon;
    }

    public List<Artikal> getOmiljeniArtikli() {
        return omiljeniArtikli;
    }

    public void setOmiljeniArtikli(List<Artikal> omiljeniArtikli) {
        this.omiljeniArtikli = omiljeniArtikli;
    }

    public List<Korpa> getKupljeneKorpe() {
        return kupljeneKorpe;
    }

    public void setKupljeneKorpe(List<Korpa> kupljeneKorpe) {
        this.kupljeneKorpe = kupljeneKorpe;
    }

    public Set<Artikal> getPrethodniArtikli() {
        return prethodniArtikli;
    }

    public void setPrethodniArtikli(Set<Artikal> prethodniArtikli) {
        this.prethodniArtikli = prethodniArtikli;
    }
}
