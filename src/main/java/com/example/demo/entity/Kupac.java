package com.example.demo.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Kupac implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "korisnickoime", unique = true)
    protected String korIme;

    @Column
    protected String lozinka;

    @Column
    protected String ime;

    @Column
    protected String prezime;

    @Column
    protected String uloga;

    @Column
    protected String telefon;

    @Column
    protected String email;

    @Column
    protected String grad;

    @Column
    protected String adresa;

    @Column(name = "dostavljakorpu")
    protected Long dostavljaKorpu;

    @Column
    protected Long kupon;

    @ManyToMany
    @JoinTable(name = "omiljeni", joinColumns = @JoinColumn(name = "kupac_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "artikal_id",referencedColumnName = "id"))
    private List<Artikal> omiljeniArtikli = new ArrayList<Artikal>();

    @ManyToMany
    @JoinTable(name = "kupljeneKorpe", joinColumns = @JoinColumn(name = "kupac_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "korpa_id",referencedColumnName = "id"))
    private List<Korpa> kupljeneKorpe = new ArrayList<Korpa>();

    @ManyToMany
    @JoinTable(name = "prethodni", joinColumns = @JoinColumn(name = "kupac_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "artikal_id",referencedColumnName = "id"))
    private Set<Artikal> prethodniArtikli = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "korpa_id")
    private Korpa korpaKupac;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Long getDostavljaKorpu() {
        return dostavljaKorpu;
    }

    public void setDostavljaKorpu(Long dostavljaKorpu) {
        this.dostavljaKorpu = dostavljaKorpu;
    }

    public Long getKupon() {
        return kupon;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public void setKupon(Long kupon) {
        this.kupon = kupon;
    }

    public List<Artikal> getOmiljeniArtikli() {
        return omiljeniArtikli;
    }

    public void setOmiljeniArtikli(List<Artikal> omiljeniArtikli) {
        this.omiljeniArtikli = omiljeniArtikli;
    }

    public List<Korpa> getKupljeneKorpe() {
        return kupljeneKorpe;
    }

    public void setKupljeneKorpe(List<Korpa> kupljeneKorpe) {
        this.kupljeneKorpe = kupljeneKorpe;
    }

    public Set<Artikal> getPrethodniArtikli() {
        return prethodniArtikli;
    }

    public void setPrethodniArtikli(Set<Artikal> prethodniArtikli) {
        this.prethodniArtikli = prethodniArtikli;
    }

    public Korpa getKorpaKupac() {
        return korpaKupac;
    }

    public void setKorpaKupac(Korpa korpaKupac) {
        this.korpaKupac = korpaKupac;
    }

    public  void dodajKupljenuKorpu(Korpa korpa){
        this.kupljeneKorpe.add(korpa);
    }

    public Kupac(){
    }

    public Kupac(String korIme, String lozinka, String ime, String prezime, String telefon, String email, String grad, String adresa) {
        this.korIme = korIme;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.telefon = telefon;
        this.email = email;
        this.grad = grad;
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Kupac{" +
                "id=" + id +
                ", korIme='" + korIme + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                ", grad='" + grad + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
}