package com.example.demo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Artikal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String naziv;

    @Column
    private String opis;

    @Column
    private int cena;

    @Column
    private int kolicina;

    @Column
    private String kategorija;

    @Column boolean popust;

    @ManyToMany(mappedBy = "omiljeniArtikli")
    private Set<Kupac> omiljeniArtikli = new HashSet<>();

    @ManyToMany(mappedBy = "prethodniArtikli")
    private Set<Kupac> prethodniKupci = new HashSet<>();

    @ManyToOne
    private Korpa korpa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public boolean isPopust() {
        return popust;
    }

    public void setPopust(boolean popust) {
        this.popust = popust;
    }

    public Set<Kupac> getOmiljeniArtikli() {
        return omiljeniArtikli;
    }

    public void setOmiljeniArtikli(Set<Kupac> omiljeniArtikli) {
        this.omiljeniArtikli = omiljeniArtikli;
    }

    public Korpa getKorpa() {
        return korpa;
    }

    public void setKorpa(Korpa korpa) {
        this.korpa = korpa;
    }

    public Set<Kupac> getPrethodniKupci() {
        return prethodniKupci;
    }

    public void setPrethodniKupci(Set<Kupac> prethodniKupci) {
        this.prethodniKupci = prethodniKupci;
    }

    @Override
    public String toString() {
        return "Artikal{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                ", opis='" + opis + '\'' +
                ", cena=" + cena +
                ", kolicina=" + kolicina +
                ", kategorija='" + kategorija + '\'' +
                ", popust=" + popust +
                ", omiljeniArtikli=" + omiljeniArtikli +
                ", prethodniKupci=" + prethodniKupci +
                ", korpa=" + korpa +
                '}';
    }

    public Artikal() {
    }

    public Artikal(String naziv, String opis, int cena, int kolicina, String kategorija) {
        this.naziv = naziv;
        this.opis = opis;
        this.cena = cena;
        this.kolicina = kolicina;
        this.kategorija = kategorija;
    }
}