package com.example.demo.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Dostavljac implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column( unique = true)
    protected String korIme;

    @Column
    protected String lozinka;

    @Column
    protected String ime;

    @Column
    protected String prezime;

    @Column
    protected String uloga;

    @Column
    protected String telefon;

    @Column
    protected String grad;

    @Column
    protected String email;

    @Column
    protected String adresa;

    @OneToMany(mappedBy = "dostavljac",cascade = CascadeType.ALL)
    private Set<Korpa> korpe = new HashSet<>();

    public Dostavljac(String korIme, String lozinka, String ime, String prezime, String dostavljac, String telefon, String email, String grad, String adresa) {
        this.korIme = korIme;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.uloga = uloga;
        this.telefon = telefon;
        this.grad = grad;
        this.email = email;
        this.adresa = adresa;
    }

    public Set<Korpa> getKorpe() {
        return korpe;
    }

    public void setKorpe(Set<Korpa> korpe) {
        this.korpe = korpe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Dostavljac() {
    }

    public Dostavljac(Long id, String korIme, String lozinka, String ime, String prezime, String uloga, String telefon, String grad, String email, String adresa) {
        this.id = id;
        this.korIme = korIme;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.uloga = uloga;
        this.telefon = telefon;
        this.grad = grad;
        this.email = email;
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Dostavljac{" +
                "id=" + id +
                ", korIme='" + korIme + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon='" + telefon + '\'' +
                ", grad='" + grad + '\'' +
                ", email='" + email + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
}
