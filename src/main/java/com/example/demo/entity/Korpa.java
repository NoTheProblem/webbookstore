package com.example.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Korpa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    protected String stanje;

    @Column
    private Date datum;

    @Column
    private int cenaKorpe;

    @Column
    private String datumstr;

    @Column
    private Long idDostavljaca;

    @ManyToOne
    private Dostavljac dostavljac;

    @OneToOne(mappedBy = "korpaKupac", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private Kupac kupac;

    @ManyToMany
    @JoinTable(name = "listaArtikla", joinColumns = @JoinColumn(name = "kupac_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "artikal_id",referencedColumnName = "id"))

    private List<Artikal> lista = new ArrayList<Artikal>();

    public Dostavljac getDostavljac() {
        return dostavljac;
    }

    public void setDostavljac(Dostavljac dostavljac) {
        this.dostavljac = dostavljac;
    }

    public Long getIdDostavljaca() {
        return idDostavljaca;
    }

    public void setIdDostavljaca(Long idDostavljaca) {
        this.idDostavljaca = idDostavljaca;
    }

    public List<Artikal> getLista() {
        return lista;
    }

    public void setLista(Artikal oA) {
        this.lista.add(oA);
    }

    public void setKorpa(List<Artikal> korpa){
        this.lista=korpa;
    }

    public int getCenaKorpe() {
        return cenaKorpe;
    }

    public void setCenaKorpe(int cenaKorpe) {
        this.cenaKorpe = cenaKorpe;
    }

    public void setStanje(String korIme) {
        this.stanje = korIme;
    }

    public String getStanje() {
        return stanje;
    }

    public void setDate(Date d) {
        this.datum = d;
    }

    public Date getDate() {
        return datum;
    }

    public void setDatestr() {
        this.datumstr = datum.toString();
    }

    public String getDatestr()
    {
        return datumstr;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int nadjiCenuKorpe(){
        int cena = 0;
        for(Artikal lis: lista){
            cena+=lis.getCena();
        }
        return cena;
    }
}