package com.example.demo.service;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Dostavljac;
import com.example.demo.entity.Korpa;
import com.example.demo.entity.Kupac;

import java.util.List;

public interface WebService {

    Kupac create(Kupac kupac) throws Exception;

    Kupac findOneKupac(Long id);

    Dostavljac findOneDostavljac(Long id);

    List<Kupac> findUPKupac(String imeKorisnika, String Lozinka);

    List<Dostavljac> findUPDostavljac(String imeKorisnika, String Lozinka);

    List<Kupac> findUKupac(String imeKorisnika);

    List<Dostavljac> findUDostavljac(String korIme);

    void deleteKupac(Long id);

    List<Kupac> findAllKupac();

    void uploadArtikal(Artikal A, long id);

    Artikal findOneArtikal(Long id);

    void deleteArtikal(Long id);

    void deleteDostavljac(Long id);

    void dobavljaj(long idd, long idk);

    void dostavi(long idd, long idk);

    List<Korpa> sveKorpeStanje(String stanje);

    boolean setDodajUlistu(long idk, long ida);

    void setOmiljeniArtikal(long idk, long ida);

    List<Dostavljac> findAllDostavljac();

    List<Artikal> findAllArtikal();

    List<Artikal> findByKategorija(String kategorija);

    List<Artikal> findOmiljeneArtikle(Long idk);
}