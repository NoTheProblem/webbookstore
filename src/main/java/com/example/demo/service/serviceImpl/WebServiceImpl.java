package com.example.demo.service.serviceImpl;

import com.example.demo.entity.Artikal;
import com.example.demo.entity.Dostavljac;
import com.example.demo.entity.Korpa;
import com.example.demo.entity.Kupac;
import com.example.demo.repository.ArtikalRepository;
import com.example.demo.repository.DostavljacRepository;
import com.example.demo.repository.KorpaRepository;
import com.example.demo.repository.KupacRepository;
import com.example.demo.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WebServiceImpl implements WebService {

    @Autowired
    private DostavljacRepository dostavljacRepository;

    @Autowired
    private KupacRepository kupacRepository;

    @Autowired
    private ArtikalRepository artikalRepository;

    @Autowired
    private KorpaRepository korpaRepository;

    @Override
    public Kupac create(Kupac kupac) throws Exception {
        if(kupac.getId() != null){
            throw new Exception("ID must be null!");
        }
        kupac.setDostavljaKorpu((long)-1);
        kupac.setKupon((long) 0);
        kupac.setUloga("kupac");
        Korpa korpa = new Korpa();
        korpa.setStanje("Napravljena");
        Date date = new Date();
        korpa.setDate(date);
        korpaRepository.save(korpa);
        kupac.setKorpaKupac(korpa);
        Kupac newKupac = kupacRepository.save(kupac);
        return  newKupac;
    }

    @Override
    public Kupac findOneKupac(Long id) {
        Kupac k = kupacRepository.getOne(id);
        return  k;
    }

    @Override
    public Dostavljac findOneDostavljac(Long id){
        Dostavljac d = dostavljacRepository.getOne(id);
        return d;
    }

    @Override
    public List<Kupac> findUPKupac(String imeKorisnika, String Lozinka) {
        List<Kupac> k = kupacRepository.findByKorImeAndLozinka(imeKorisnika, Lozinka);
        return k;
    }
    @Override
    public List<Dostavljac> findUPDostavljac(String imeKorisnika, String Lozinka){
        List<Dostavljac> d = dostavljacRepository.findByKorImeAndLozinka(imeKorisnika, Lozinka);
        return d;
    }

    @Override
    public List<Kupac> findUKupac(String imeKorisnika) {
        List<Kupac> k = kupacRepository.findByKorIme(imeKorisnika);
        return k;
    }

    @Override
    public List<Dostavljac> findUDostavljac(String korIme){
        List<Dostavljac> d = dostavljacRepository.findByKorIme(korIme);
        return d;
    }

    @Override
    public void deleteKupac(Long id) {
        Kupac k = kupacRepository.getOne(id);
        kupacRepository.delete(k);
    }

    @Override
    public List<Kupac> findAllKupac() {
        List<Kupac> kupci = kupacRepository.findAll();
        return  kupci;
    }

    @Override
    public void uploadArtikal(Artikal A, long id) {
        Artikal artikal = findOneArtikal(id);
        artikal=A;
        artikal.setId(id);
        artikalRepository.save(artikal);
    }

    @Override
    public Artikal findOneArtikal(Long id) {
        Artikal A = artikalRepository.getOne(id);
        return A;
    }

    @Override
    public void deleteArtikal(Long id) {
        Artikal A = artikalRepository.getOne(id);
        artikalRepository.delete(A);
    }

    @Override
    public void deleteDostavljac(Long id){
        Dostavljac d = dostavljacRepository.getOne(id);
        dostavljacRepository.delete(d);
    }

    @Override
    public void dobavljaj(long idd, long idk) {
        Kupac kupac = kupacRepository.getOne(idd);
        Korpa korpa =korpaRepository.getOne(idk);
        korpa.setStanje("Dostava u toku");
        korpa.setIdDostavljaca(idd);
        korpaRepository.save(korpa);
        kupac.setDostavljaKorpu(idd);
        kupacRepository.save(kupac);
    }

    @Override
    public void dostavi(long idd, long idk) {
        Kupac kupac = kupacRepository.getOne(idd);
        Korpa korpa = korpaRepository.getOne(idk);
        korpa.setStanje("Dostavljeno");
        kupac.setDostavljaKorpu((long) -1);
        kupac.dodajKupljenuKorpu(korpa);
        korpaRepository.save(korpa);
        kupacRepository.save(kupac);
    }

    @Override
    public List<Korpa> sveKorpeStanje(String stanje) {
        List<Korpa> k = korpaRepository.findAll();
        List<Korpa> korpa = new ArrayList<Korpa>();
        for(Korpa K: k){
            K.setDatestr();
            korpaRepository.save(K);
            if(K.getStanje().equals(stanje)){
                korpa.add(K);
            }
        }
        return korpa;
     }

    @Override
    public boolean setDodajUlistu(long idk, long ida) {
        Artikal a = artikalRepository.getOne(ida);
        Kupac k = kupacRepository.getOne(idk);
        a.setKolicina(a.getKolicina()-1);
        if(a.getKolicina()<0){
            return false;
        }
        k.getKorpaKupac().getLista().add(a);
        korpaRepository.save(k.getKorpaKupac());
        kupacRepository.save(k);
        return true;
    }

    @Override
    public void setOmiljeniArtikal(long idk, long ida) {
        Artikal a = artikalRepository.getOne(ida);
        Kupac k = kupacRepository.getOne(idk);
        List<Artikal> omiljeniA = k.getOmiljeniArtikli();
        omiljeniA.add(a);
        k.setOmiljeniArtikli(omiljeniA);
        kupacRepository.save(k);
    }

    @Override
    public List<Dostavljac> findAllDostavljac(){
        List<Dostavljac> d = dostavljacRepository.findAll();
        return d;
    }

    @Override
    public List<Artikal> findAllArtikal() {
        List<Artikal> a = artikalRepository.findAll();
        return a;
    }

    @Override
    public List<Artikal> findByKategorija(String kategorija) {
        List<Artikal> a = artikalRepository.findByKategorija(kategorija);
        return a;
    }

    @Override
    public List<Artikal> findOmiljeneArtikle(Long idk){
        Kupac k = kupacRepository.getOne(idk);
        List<Artikal> omiljeni =  k.getOmiljeniArtikli();
        return omiljeni;
    }
}