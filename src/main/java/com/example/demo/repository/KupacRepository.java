package com.example.demo.repository;

import com.example.demo.entity.Kupac;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface KupacRepository extends JpaRepository<Kupac, Long> {
    List<Kupac> findByKorImeAndLozinka(String korIme, String Lozinka);
    List<Kupac> findByKorIme(String imeKorisnika);
}
