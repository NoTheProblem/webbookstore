package com.example.demo.repository;

import com.example.demo.entity.Dostavljac;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DostavljacRepository extends JpaRepository<Dostavljac, Long> {
    List<Dostavljac> findByKorImeAndLozinka(String korIme, String Lozinka);

    List<Dostavljac> findByKorIme(String korIme);
}