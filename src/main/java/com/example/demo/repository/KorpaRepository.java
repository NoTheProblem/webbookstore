package com.example.demo.repository;

import com.example.demo.entity.Korpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KorpaRepository extends JpaRepository<Korpa, Long> {
}
