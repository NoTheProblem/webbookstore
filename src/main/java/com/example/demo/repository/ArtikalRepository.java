package com.example.demo.repository;

import com.example.demo.entity.Artikal;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ArtikalRepository extends JpaRepository<Artikal, Long> {
    List<Artikal> findByKategorija(String kategorija);
}