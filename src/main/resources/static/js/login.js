$(document).on("submit", "form", function (event) {
    event.preventDefault();

    var korIme = $("#korIme").val();
    var sifra= $("#sifra").val();
    var uloga = "nema"
    var newEmployeeJSON = formToJSON(korIme,sifra, uloga);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/login",
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function (response) {
            if(response['uloga']=="nema"){
                alert("Greska pre loginovanju!");
            }
            else{
               alert("Dobrodosli " + korIme);
               localStorage.setItem("korIme", response['korIme']);
               localStorage.setItem("uloga", response['uloga']);
               window.location.href = "index.html";
            }
        },
        error: function (data) {
            alert("Greška!");
        }
    });
});

function formToJSON(korIme,sifra, uloga) {
    return JSON.stringify({
        "korIme": korIme,
        "sifra": sifra,
        "uloga": uloga
    });
}