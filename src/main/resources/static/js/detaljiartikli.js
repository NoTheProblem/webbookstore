//Otvaranje stanice Kategorije

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/kategorija",
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['naziv'] + "</td>";
                row += "<td>" + data[i]['cena'] + "</td>";
                row += "<td>" + data[i]['kategorija'] + "</td>";
                row += "<td>" + data[i]['opis'] + "</td>";
                row += "<td>" + data[i]['kolicina'] + "</td>";
                if(data[i]['popust']){
                    row += "<td>" + "Akcija!" + "</td>";
                }else{
                    row += "<td>" + ""   + "</td>";
                }
                var btn = "<button class='btnSeeMore' id='omiljeno/" + data[i]['id'] + "'>dodaj </button>";
                row += "<td class='omiljeno'> " + btn + "</td>";
                var btn2 ="<button class='btnSeeMore1' id='korpa/" + data[i]['id']+ "'>dodaj </button>";
                row += "<td class='omiljeno'> " +btn2 + "</td>";
                row += "</tr>";
                $('#artikli').append(row);
            }

        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Dodavanje artikla u omiljene

$(document).on('click', '.btnSeeMore', function () {
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/kategorija/" + this.id,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
           alert("Artikal dodat u Omiljene!");
        },
        error: function (data) {
            alert("Niste prijavljeni!");
        }
    });
});

//Dodavanje artikla u korpu

$(document).on('click', '.btnSeeMore1', function () {
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/kategorija/" + this.id,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
           alert("Artikal dodat u Korpu!");
           window.location.href = "kategorija.html";
        },
        error: function (data) {
            alert("Nema vise artikala/Niste prijavljeni");
        }
    });
});

function formToJSON(korIme) {
    return JSON.stringify({
        "korIme": korIme
    });
}

function bale(){
    var table = document.getElementById("artikli");
        for(var i = 1; i <= table.rows.length; i++)
        {
            if(table.rows[i].cells.item(4).innerHTML!="beletristika"){
               table.rows[i].style.display='none';

            }
        }
}

function strucna(){
    var table = document.getElementById("artikli");
        for(var i = 1; i <= table.rows.length; i++)
        {
            if(table.rows[i].cells.item(4).innerHTML!="strucna"){
               table.rows[i].style.display='none';
            }
        }
}

function drama(){
    var table = document.getElementById("artikli");
        for(var i = 1; i <= table.rows.length; i++)
        {
           if(table.rows[i].cells.item(4).innerHTML!="drama"){
           table.rows[i].style.display='none';
           }
    }
}

function sortTableNAME() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("artikli");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[1];
      y = rows[i + 1].getElementsByTagName("TD")[1];
      // Check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        // If so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableCENA() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("artikli");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[2];
      y = rows[i + 1].getElementsByTagName("TD")[2];
      //check if the two rows should switch place:
      if (Number(x.innerHTML) > Number(y.innerHTML)) {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function pokazipopust(){
    var table = document.getElementById("artikli");
    for(var i = 1; i <= table.rows.length; i++)
    {
        if(table.rows[i].cells.item(6).innerHTML!="Akcija!"){
           table.rows[i].style.display='none';

        }
    }
}

function search() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("artikli");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
   }
   function searchO() {
         // Declare variables
         var input, filter, table, tr, td, i, txtValue;
         input = document.getElementById("myInputO");
         filter = input.value.toUpperCase();
         table = document.getElementById("artikli");
         tr = table.getElementsByTagName("tr");

         // Loop through all table rows, and hide those who don't match the search query
         for (i = 0; i < tr.length; i++) {
           td = tr[i].getElementsByTagName("td")[3];
           if (td) {
             txtValue = td.textContent || td.innerText;
             if (txtValue.toUpperCase().indexOf(filter) > -1) {
               tr[i].style.display = "";
             } else {
               tr[i].style.display = "none";
             }
           }
         }
 }

function minsearch(){
    var m= parseInt( $('#min').val(), 10 );
    var table = document.getElementById("artikli");
        for(var i = 1; i <= table.rows.length; i++)
        {
            if(table.rows[i].cells.item(2).innerHTML<m){
               table.rows[i].style.display='none';
            }else{
               table.rows[i].style.display = "";
            }
        }
}

function maxsearch(){
    var m= parseInt( $('#max').val(), 10 );
    var table = document.getElementById("artikli");
        for(var i = 1; i <= table.rows.length; i++)
        {
            if(table.rows[i].cells.item(2).innerHTML>m){
               table.rows[i].style.display='none';
            }else{
               table.rows[i].style.display = "";
            }
        }
}