//Prikaz artikala

$(document).on('click', '.ArtikalP', function () {
    tabelerestart();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th colspan='8'> Artikli</th></tr><tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Opis</th><th>Kategorija</th><th   colspan='3'>Kolicina</th></tr>";
            $('#artikli').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['naziv'] + "</td>";
                row += "<td>" + data[i]['cena'] + "</td>";
                row += "<td>" + data[i]['kategorija'] + "</td>";
                row += "<td>" + data[i]['opis'] + "</td>";
                row += "<td>" + data[i]['kolicina'] + "</td>";
                var btn1 ="<button class='Delete' id='/deleteA/" + data[i]['id']+ "'>Obrisi artikal</button>";
                row += "<td > " +btn1 + "</td>";
                var btn2 ="<button class='ChangeA' id='/changeA/" + data[i]['id']+ "'>Izmeni artikal</button>";
                row += "    <td > " +btn2 + "</td>";
                row += "</tr>";
                $('#artikli').append(row);
            }
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Prikaz kupaca

$(document).on('click', '.KupacP', function () {
    tabelerestart();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th colspan='11'> Kupci</th></tr><tr><th>ID</th><th>Korisnicko ime</th><th>Ime</th><th>Prezime</th><th>Telefon</th><th>Email</th><th>Grad</th><th  colspan='4'>Adresa</th></tr>";
            $('#korisnik').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['korIme'] + "</td>";
                row += "<td>" + data[i]['ime'] + "</td>";
                row += "<td>" + data[i]['prezime'] + "</td>";
                row += "<td>" + data[i]['telefon'] + "</td>";
                row += "<td>" + data[i]['email'] + "</td>";
                row += "<td>" + data[i]['grad'] + "</td>";
                row += "<td>" + data[i]['adresa'] + "</td>";
                var btn1 ="<button class='Delete' id='/deleteK/" + data[i]['id']+ "'>Obrisi korisnika</button>";
                row += "<td > " +btn1 + "</td>";
                var btn2 ="<button class='ChangeKupac' id='/changeK/" + data[i]['id']+ "'>Izmeni korisnika</button>";
                row += "    <td > " +btn2 + "</td>";
                var btn3 ="<button class='changeUlogaK' id='/changeUlogaK/" + data[i]['id']+ "'>Promeni ulogu</button>";
                row += "    <td > " +btn3 + "</td>";
                row += "</tr>";
                $('#korisnik').append(row);
            }
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Prikaz dostavljaca

$(document).on('click', '.DostavljacP', function () {
    tabelerestart();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th colspan='11'> Dostavljac</th></tr><tr><th>ID</th><th>Korisnicko ime</th><th>Ime</th><th>Prezime</th><th>Telefon</th><th>Email</th><th>Grad</th><th colspan='4'>Adresa</th></tr>";
            $('#dostavljac').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['korIme'] + "</td>";
                row += "<td>" + data[i]['ime'] + "</td>";
                row += "<td>" + data[i]['prezime'] + "</td>";
                row += "<td>" + data[i]['telefon'] + "</td>";
                row += "<td>" + data[i]['email'] + "</td>";
                row += "<td>" + data[i]['grad'] + "</td>";
                row += "<td>" + data[i]['adresa'] + "</td>";
                var btn1 ="<button class='Delete' id='/deleteD/" + data[i]['id']+ "'>Obrisi korisnika</button>";
                row += "<td > " +btn1 + "</td>";
                var btn2 ="<button class='ChangeD' id='/changeD/" + data[i]['id']+ "'>Izmeni korisnika</button>";
                row += "    <td > " +btn2 + "</td>";
                var btn3 ="<button class='Uloga' id='/changeUlogaD/" + data[i]['id']+ "'>Promeni ulogu</button>";
                row += "    <td > " +btn3 + "</td>";
                row += "</tr>";
                $('#dostavljac').append(row);
            }
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

// Delete za sve

$(document).on('click', '.Delete', function () {
    $.ajax({
        url: "http://localhost:8080/admin" +this.id,
        type: "DELETE",
        dataType: "json",
        success: function(data) {
           alert("Obrisano! ");
           console.log("obrisano");
           window.location.href = "admin.html";
        },
        error: function (data) {
           alert("Obrisano! ");
           console.log("obrisano");
           window.location.href = "admin.html";
        }
    });
});

//Kupac - > Dostavljac

$(document).on('click', '.changeUlogaK', function () {
     var korIme = localStorage.getItem("korIme");
     var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/admin" + this.id ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
            alert("Promenjena uloga");
            window.location.href = "admin.html";
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Dostavljac -> Kupac

$(document).on('click', '.Uloga', function () {
     var korIme = localStorage.getItem("korIme");
     var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/admin" + this.id ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
            alert("Promenjena uloga");                 // prikaži taj element
            window.location.href = "admin.html";
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

function formToJSON(korIme) {
    return JSON.stringify({
        "korIme": korIme

    });
}

function formToJSON1(naziv,opis,cena,kolicina,kategorija) {
    return JSON.stringify({
        "naziv": naziv,
        "opis": opis,
        "cena": cena,
        "kolicina": kolicina,
        "kategorija": kategorija
    });
}

function dodajArtikal(){
    document.getElementById("addArtikal").style.display = "block";
}

//Prikaz izvestaja

$(document).on('click', '.Izvestaj', function () {
    tabelerestart();
    var i = this.id;
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/izvestaj/" +this.id,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var c = 0;
            var k = "<thead><tr><th colspan='3'> "+i+" izvestaj</th></thead><tbody><tr><th>ID</th><th>Datum</th><th>Cena</th></tr>";
            $('#izvestaj').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['datum'] + "</td>";
                row += "<td>" + data[i]['cenaKorpe'] + "</td>";
                row += "</tr>";
                $('#izvestaj').append(row);
                c = c + data[i]['cenaKorpe'];
            }
            var r = "<tr><td colspan='2'>Ukupna zarada</td><td>"+c+"</td></tr>";
            $('#izvestaj').append(r);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//IDK// Dodavanje artikla IDK

 $(document).on("submit", '.dodavanje', function (event) {
         event.preventDefault();
         var naziv = $("#naziv").val();
         var opis= $("#opis").val();
         var cena= $("#cena").val();
         var kolicina= $("#kolicina").val();
         var kategorija= $("#kategorija").val();
         var newEmployeeJSON1 = formToJSON1(naziv,opis,cena,kolicina,kategorija);
         $.ajax({
             type: "POST",
             url: "http://localhost:8080/admin/" + this.id,
             dataType: "json",
             contentType: "application/json",
             data: newEmployeeJSON1,
             success: function () {
                 alert("Uspesno dodat artikal " + naziv);
                 window.location.href = "admin.html";
             },
             error: function (data) {
                 alert("Greška!");
             }
         });
     });

function formToJSON(korIme) {
    return JSON.stringify({
        "korIme": korIme

    });
}

function adminauth() {
    var c = localStorage.getItem("uloga");
    if(c=="admin"){
    }
    else{
        alert("Nemate pravo pristupa ovoj stranici kao " + c);
        window.location.href = "index.html";
    }
}

//Promena artikla get

$(document).on('click', '.ChangeA', function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            var k = "<label for='naziv1'>Naziv:</label>";
            k+="<input id='naziv1' type='text' value='"+data['naziv']+"'/><br>";
            k+="<label for='opis1'>Opis:</label>";
            k+="<input id='opis1' type='text' value='"+data['opis']+"'/><br>" ;
            k+="<label for='cena1'>Cena:</label>";
            k+="<input id='cena1' type='text' value='"+data['cena']+"'/><br>";
            k+="<label for='kolicina1'>Kolicina:</label>";
            k+="<input id='kolicina1' type='text' value='"+data['kolicina']+"'/><br>";
            k+="<label for='kategorija1'>Kategorija:</label>";
            k+="<input id='kategorija1' type='text' value='"+data['kategorija']+"'/><br>";
            k+="<input id='id1' type='text' value='"+data['id']+"' style='display:none'/><br>";
            k+="<button type='submit' >Promeni</button>";
            $('#changeArtikal').append(k);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Promena artikla post

$(document).on("submit", '.change', function (event) {
    event.preventDefault();
    var id = $('#id1').val();
    var naziv = $("#naziv1").val();
    var opis= $("#opis1").val();
    var cena= $("#cena1").val();
    var kolicina= $("#kolicina1").val();
    var kategorija= $("#kategorija1").val();
    var newEmployeeJSON1 = formToJSON2(id,naziv,opis,cena,kolicina,kategorija);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/admin/" + this.id,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON1,
        success: function () {
            alert("Uspesno promenjen artikal " + naziv);
            window.location.href = "admin.html";
        },
        error: function (data) {
            alert("Greška!");
        }
    });
});

function formToJSON2(id,naziv,opis,cena,kolicina,kategorija) {
    return JSON.stringify({
        "id":id,
        "naziv": naziv,
        "opis": opis,
        "cena": cena,
        "kolicina": kolicina,
        "kategorija": kategorija
    });
}

function formToJSON6(id,korIme,email,lozinka,ime,prezime,telefon,grad,adresa) {
    return JSON.stringify({
        "id" : id,
        "korIme": korIme,
        "email": email,
        "lozinka": lozinka,
        "ime": ime,
        "prezime": prezime,
        "telefon": telefon,
        "grad": grad,
        "adresa": adresa

    });
}
//Promena dostavljaca get

$(document).on('click', '.ChangeD', function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            var k = "<label for='korIme'>korIme:</label>";
            k+="<input id='korIme' type='text' value='"+data['korIme']+"'/><br>";
            k+="<label for='lozinka'>lozinka:</label>";
            k+="<input id='lozinka' type='text' value='"+data['lozinka']+"'/><br>" ;
            k+="<label for='ime'>ime:</label>";
            k+="<input id='ime' type='text' value='"+data['ime']+"'/><br>";
            k+="<label for='prezime'>prezime:</label>";
            k+="<input id='prezime' type='text' value='"+data['prezime']+"'/><br>";
            k+="<label for='telefon'>telefon:</label>";
            k+="<input id='telefon' type='text' value='"+data['telefon']+"'/><br>";
            k+="<label for='grad'>grad:</label>";
            k+="<input id='grad' type='text' value='"+data['grad']+"'/><br>";
            k+="<label for='email'>email:</label>";
            k+="<input id='email' type='text' value='"+data['email']+"'/><br>";
            k+="<label for='adresa'>adresa:</label>";
            k+="<input id='adresa' type='text' value='"+data['adresa']+"'/><br>";
            k+="<input id='id' type='text' value='"+data['id']+"' style='display:none'/><br>";
            k+="<button type='submit' >Promeni</button>";
            $('#changeDostavljac').append(k);

        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Promena kupca TODO

$(document).on('click', '.ChangeKupac', function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/admin/" +this.id,
        dataType: "json",
        success: function (data) {
            var k = "<label for='korIme'>korIme:</label>";
            k+="<input id='korIme' type='text' value='"+data['korIme']+"'/><br>";
            k+="<label for='lozinka'>lozinka:</label>";
            k+="<input id='lozinka' type='text' value='"+data['lozinka']+"'/><br>" ;
            k+="<label for='ime'>ime:</label>";
            k+="<input id='ime' type='text' value='"+data['ime']+"'/><br>";
            k+="<label for='prezime'>prezime:</label>";
            k+="<input id='prezime' type='text' value='"+data['prezime']+"'/><br>";
            k+="<label for='telefon'>telefon:</label>";
            k+="<input id='telefon' type='text' value='"+data['telefon']+"'/><br>";
            k+="<label for='grad'>grad:</label>";
            k+="<input id='grad' type='text' value='"+data['grad']+"'/><br>";
            k+="<label for='email'>email:</label>";
            k+="<input id='email' type='text' value='"+data['email']+"'/><br>";
            k+="<label for='adresa'>telefon:</label>";
            k+="<input id='adresa' type='text' value='"+data['adresa']+"'/><br>";
            k+="<input id='id' type='text' value='"+data['id']+"' style='display:none'/><br>";
            k+="<button type='submit' >Promeni</button>";
            $('#changeKupac').append(k);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});
//Promena dostavljaca post

$(document).on("submit", '.changeD', function (event) {
    event.preventDefault();
    var id = $("#id").val();
    var korIme = $("#korIme").val();
    var email= $("#email").val();
    var lozinka= $("#lozinka").val();
    var ime= $("#ime").val();
    var prezime= $("#prezime").val();
    var telefon= $("#telefon").val();
    var grad= $("#grad").val();
    var adresa= $("#adresa").val();

    var newEmployeeJSON6 = formToJSON6(id,korIme,email,lozinka,ime,prezime,telefon,grad,adresa);
    console.log(newEmployeeJSON6);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/admin/changeDostavljac"  ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON6,
        success: function () {
            alert("Uspesno promenjen dostavljac ");
            window.location.href = "admin.html";
        },
        error: function (data) {
            alert("Greška!");
        }
    });
});

//promena kupca TODO

$(document).on("submit", '.changeK', function (event) {
    event.preventDefault();
    var id = $("#id").val();
    var korIme = $("#korIme").val();
    var email= $("#email").val();
    var lozinka= $("#lozinka").val();
    var ime= $("#ime").val();
    var prezime= $("#prezime").val();
    var telefon= $("#telefon").val();
    var grad= $("#grad").val();
    var adresa= $("#adresa").val();

    var newEmployeeJSON7 = formToJSON6(id,korIme,email,lozinka,ime,prezime,telefon,grad,adresa);
    console.log(newEmployeeJSON7);
 $.ajax({
        type: "POST",
        url: "http://localhost:8080/admin/changeKupac"  ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON7,
        success: function () {
            alert("Uspesno promenjen kupac ");
            window.location.href = "admin.html";
        },
        error: function (data) {
            alert("Greška!");
        }
    });
});

function tabelerestart(){
    $("#dodajartikalT tr").remove();
    $("#artikli tr").remove();
    $("#korisnik tr").remove();
    $("#dostavljac tr").remove();
    $("#izvestaj tr").remove();
}