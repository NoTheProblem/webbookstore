$(document).on("submit", "form", function (event) {
    event.preventDefault();
    var korIme = $("#korisnickoIme").val();
    var email= $("#email").val();
    var lozinka= $("#sifra").val();
    var ime= $("#ime").val();
    var prezime= $("#prezime").val();
    var telefon= $("#telefon").val();
    var grad= $("#grad").val();
    var adresa= $("#adresa").val();
    var newEmployeeJSON = formToJSON(korIme,email,lozinka,ime,prezime,telefon,grad,adresa);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/kupci",
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
            alert("Uspesna registracija! " + korIme);
            window.location.href = "login.html";
        },
        error: function (data) {
            alert("Greška!");
        }
    });
});

function formToJSON(korIme,email,lozinka,ime,prezime,telefon,grad,adresa) {
    return JSON.stringify({
        "korIme": korIme,
        "email": email,
        "lozinka": lozinka,        
        "ime": ime,
        "prezime": prezime,
        "telefon": telefon,
        "grad": grad,
        "adresa": adresa
    });
}