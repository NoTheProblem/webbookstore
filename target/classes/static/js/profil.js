var kuponcic;

//Prikaz Omiljenih Artikala

$(document).on('click', '.btnSeeMoreO', function () {
    tabelerestart();
    var korIme = localStorage.getItem("korIme");
    var employeesDiv = $("#korpa");
    employeesDiv.hide();
    var employeesDiv1 = $("#omiljeni");
    employeesDiv1.show();
    var newEmployeeJSON = formToJSON(korIme);
    console.log(this.id);
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/profil/omiljeni/" +korIme,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th   colspan='5'>Omiljeni Artikli</th></tr><tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Opis</th><th>Kategorija</th></tr>";
            $('#omiljeni').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['naziv'] + "</td>";
                row += "<td>" + data[i]['cena'] + "</td>";
                row += "<td>" + data[i]['kategorija'] + "</td>";
                row += "<td>" + data[i]['opis'] + "</td>";
                row += "</tr>";
                $('#omiljeni').append(row);
            }

        },
        error: function (data) {
            console.log("ERROR : ", data);
        }

    });

});

// Prikaz korisnikovih podataka

$(document).ready(function () {
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/profil",
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function (data) {
            console.log("SUCCESS : ", data);
            var row = "<tr>";
            row += "<td> ID </td> <td  colspan='2'> " + data['id'] + "</td> </tr>" ;
            row += "<tr><td> Korisnicko ime </td> <td  colspan='2'>" + data['korIme'] + "</td></tr>";
            row += "<tr><td> Ime </td> <td  colspan='2'> " + data['ime'] + "</td></tr>";
            row += "<tr><td> Prezime </td> <td  colspan='2'> " + data['prezime'] + "</td></tr>";
            row += "<tr><td> Telefon </td> <td  colspan='2'> " + data['telefon'] + "</td></tr>";
            row += "<tr><td> Email </td> <td  colspan='2'> " + data['email'] + "</td></tr>";
            row += "<tr><td> Grad </td> <td  colspan='2'> " + data['grad'] + "</td></tr>";
            row += "<tr><td> Adresa </td> <td  colspan='2'> " + data['adresa'] + "</td></tr>";
            row += "<tr><td> Broj kupona </td> <td  colspan='2'> " + data['kupon'] + "</td></tr>";
            var btn1 ="<button class='btnSeeMoreO' id='/omiljeni/" + data['korIme']+ "'>Omiljeni</button>";
            row += "<tr><td > " +btn1 + "</td>";
            var btn2 ="<button class='btnSeeMoreK' id='/korpa/" + data['korIme']+ "'>Korpa</button>";
            row += "<td > " +btn2 + "</td>";
            var btn3 ="<button class='btnSeeMoreI' id='/istorija/" + data['korIme']+ "'>Istorija</button>";
            row += "<td > " +btn3 + "</td>";
            row += "</tr>";
            kuponcic=data['kupon'];
            $('#korisnik').append(row);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });

});

//Prikaz Korpe

$(document).on('click', '.btnSeeMoreK', function () {
    tabelerestart();
    var employeesDiv = $("#omiljeni");
    employeesDiv.hide();
    var employeesDiv1 = $("#korpa");
    employeesDiv1.show();
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/profil/korpa/" +korIme,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th  colspan='6'>Artikli iz korpe</th><tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Opis</th><th>Kategorija</th><th>Izbaci</th></tr>";
            $('#korpa').append(k);
            var cenaKorpe = 0;
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['naziv'] + "</td>";
                row += "<td>" + data[i]['cena'] + "</td>";
                row += "<td>" + data[i]['kategorija'] + "</td>";
                row += "<td>" + data[i]['opis'] + "</td>";
                var btn1 ="<button class='btnSeeMoreD' id='/delete/" + data[i]['id']+ "'>Izbaci</button>";
                row += "<td > " +btn1 + "</td>";
                row += "</tr>";
                cenaKorpe += data[i]['cena'];
                $('#korpa').append(row);
            }
            var btn1 ="<button class='btnSeeMoreKupi' id='/kupi'>Kupi</button>";
            var roww= "<tr><td > " +btn1 + "</td><td> Cena korpe:" + cenaKorpe+"din</td></tr>";
            if(kuponcic>9){
                var btnw1w ="<button class='btnSeeMoreKupi' id='/kupi/10'>Popust 20%</button>";
                var rowwww= "<tr><td > " +btnw1w + "</td><td> Cena korpe:" + cenaKorpe*0.8+"din</td></tr>";
                $('#korpa').append(rowwww);
            }
            if(kuponcic>4){
                var btnw1 ="<button class='btnSeeMoreKupi' id='/kupi/5'>Popust 10%</button>";
                var rowww= "<tr><td > " +btnw1 + "</td><td> Cena korpe:" + cenaKorpe*0.9+"din</td></tr>";
                $('#korpa').append(rowww);
            }
            $('#korpa').append(roww);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

function formToJSON(korIme) {
    return JSON.stringify({
        "korIme": korIme

    });
}

//Kupovina Korpe

$(document).on('click', '.btnSeeMoreKupi', function () {
    var employeesDiv = $("#allEmployees");
    employeesDiv.hide();
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/profil" +this.id  ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
            alert("Poruceno, dostavljac ce vas kontaktirati!");
            window.location.href = "profil.html";
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Brisanje artikla iz korpe

$(document).on('click', '.btnSeeMoreD', function () {
     var employeesDiv = $("#allEmployees");
     employeesDiv.hide();
     var korIme = localStorage.getItem("korIme");
     var newEmployeeJSON = formToJSON(korIme);
     $.ajax({
        type: "POST",
        url: "http://localhost:8080/profil" + this.id ,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
            alert("Izbaceno");
            window.location.href = "profil.html";
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Prikaz istorije kupljenih artikala

$(document).on('click', '.btnSeeMoreI', function () {
    tabelerestart();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/profil" +this.id,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><th   colspan='5'>Istorija kupljenih artikala</th></tr><tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Opis</th><th>Kategorija</th></tr>";
            $('#istorija').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td>" + data[i]['naziv'] + "</td>";
                row += "<td>" + data[i]['cena'] + "</td>";
                row += "<td>" + data[i]['kategorija'] + "</td>";
                row += "<td>" + data[i]['opis'] + "</td>";
                row += "</tr>";
                $('#istorija').append(row);
            }
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

function tabelerestart(){
    $("#omiljeni tr").remove();
    $("#korpa tr").remove();
    $("#istorija tr").remove();
}
