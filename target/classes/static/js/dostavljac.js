//Prikaz podataka dostavljaca

$(document).ready(function () {
    var korIme = localStorage.getItem("korIme");
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/dostavljac/"+korIme,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var row = "<tr>";
            row += "<td> ID </td> <td> " + data['id'] + "</td> </tr>" ;
            row += "<tr><td> Korisnicko ime </td> <td>" + data['korIme'] + "</td></tr>";
            row += "<tr><td> Ime </td> <td> " + data['ime'] + "</td></tr>";
            row += "<tr><td> Prezime </td> <td> " + data['prezime'] + "</td></tr>";
            row += "<tr><td> Telefon </td> <td> " + data['telefon'] + "</td></tr>";
            row += "<tr><td> Email </td> <td> " + data['email'] + "</td></tr>";
            row += "<tr><td> Grad </td> <td> " + data['grad'] + "</td></tr>";
            row += "<tr><td> Adresa </td> <td> " + data['adresa'] + "</td></tr>";
            var btn1 ="<button class='btnSeeMore' id='/korpe/" + data['korIme']+ "'>Prikaz dostupnih korpi</button>";
            row += "<tr><td   colspan='2'> " +btn1 + "</td>";
            var btn2 ="<button class='btnSeeMore1' id='/preuzete/" + data['korIme']+ "'>Prikaz preuzetih korpi</button>";
            row += "<tr><td   colspan='2'> " +btn2 + "</td>";
            row += "</tr>";
            $('#korisnik').append(row);
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

//Dostupne korpe za preuzimanje

$(document).on('click', '.btnSeeMore', function () {
    tabelerestart();
   $.ajax({
        type: "GET",
        url: "http://localhost:8080/dostavljac" + this.id ,
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            var k = "<tr><td  colspan='4'>Dostupne korpe za preuzimanje</td><tr><td>ID</td><td>Datum</td><td>Preuzmi</td></tr>";
            $('#korpae').append(k);
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                row += "<td>" + data[i]['id'] + "</td>";
                row += "<td >" + data[i]['datum'] + "</td>";
                var btn1 ="<button class='btnSeeMoreP' id='/preuzmi/" + data[i]['id']+ "'>Preuzmi</button>";
                row += "<td > " +btn1 + "</td>";
                row += "</tr>";
                $('#korpae').append(row);
            }
        },
        error: function (data) {
            console.log("ERROR : ", data);
            alert("nema korpi");
        }
    });
});

//Preuzimanje korpe -> Kupljena --> Dostava u toku

$(document).on('click', '.btnSeeMoreP', function () {
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    console.log(newEmployeeJSON);
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/dostavljac" + this.id,
        dataType: "json",
        contentType: "application/json",
        data: newEmployeeJSON,
        success: function () {
           alert("Korpa preuzeta!");
           window.location.href = "dostavljac.html";
        },
        error: function (data) {
            alert("Nema vise korpi");
            console.log("ERROR : ", data);
        }
    });
});

//Korpe koje ima dostavljac -> Dostava u toku --> Dostavljena

$(document).on('click', '.btnSeeMore1', function () {
    tabelerestart();
    var korIme = localStorage.getItem("korIme");
    var newEmployeeJSON = formToJSON(korIme);
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/dostavljac" + this.id,
        dataType: "json",
        success: function (data) {
        console.log("SUCCESS : ", data);
        var k = "<tr><td   colspan='4'>Preuzete korpe</td><tr><td>ID</td><td>Datum</td><td>Dostavi</td></tr>";
        $('#korpae').append(k);
        for (i = 0; i < data.length; i++) {
            var row = "<tr>";
            row += "<td>" + data[i]['id'] + "</td>";
            row += "<td >" + data[i]['datum'] + "</td>";
            var btn1 ="<button class='btnSeeMoreP' id='/dostavi/" + data[i]['id']+ "'>Dostavi</button>";
            row += "<td > " +btn1 + "</td>";
            row += "</tr>";
            $('#korpae').append(row);
            }
        },
        error: function (data) {
            alert("Nemate vise korpi kod sebe!");
            console.log("ERROR : ", data);
        }
    });
});

function formToJSON(korIme) {
    return JSON.stringify({
        "korIme": korIme
    });
}

function dostavljacauth() {
    var c = localStorage.getItem("uloga");
    if(c=="dostavljac"){
    }
    else{
        alert("Nemate pravo pristupa ovoj stranici kao " + c);
        window.location.href = "index.html";
    }
}

function tabelerestart(){
    $("#korpae tr").remove();
}
